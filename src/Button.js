import React from "react";
import App from './App.js';

const btnStyle={
	width:'200px',
    padding:'10px 25px',
    'background-color':'#24aa17',
    color:'#ffffff',
    height:'45px',
    'font-weight':'bold',
    'margin-left': '70px',
}
class Button extends React.Component{

	constructor(props){
		super(props);
		this.state = {

			isOpen:true
		}
	}

	render(){
		const body = this.state.isOpen && <section>App</section>;

		return(

			<div class="btn-main comp" style={btnStyle}> Заказать звонок </div>);
		
	}
}
export default Button;