import React from "react";
import { connect } from 'react-redux';
import { createStore } from 'redux';
import logo from './logo.svg';
import './App.css';
const OPEN_FORM='OPEN_FORM';
const CLOSE_FORM='CLOSE_FORM';

const popupfadeStyle={
        display: 'absolute',
      };
const popupStyle={
        position: 'fixed',
        top: '20%',
        left: '25%',
        padding: '20px',
        width: '50%',
        background: '#fff',
        'z-index': '99999',
        opacity: '1', 
      };
const popupcloseStyle={
        position: 'absolute',
        top: '10px',
        right: '10px',
      };
const form_buttonStyle={
        background: '#32ab23',
        color: 'white',
        position: 'absolute',
        bottom: '10px',
        left:'50%',
        'margin-left':'-60px',
      };
const btnStyle={
        color: 'white',
      };
const fieldStyle={
        width: '100%',
        border: '0',
        padding: '5px',
        'background-color': '#e5e6ea',
      };
const fieldsStyle={
        margin: '15px 0 0 0',
      };
const commentStyle={
        margin: '20px 0 0 0',
      };
const InitialState={
  type:CLOSE_FORM
}
 const rootReducer=(state = InitialState, action)=>{
  return state
}
const changeType=(state)=>{
  switch(state.type){
    case(OPEN_FORM): return{ type: CLOSE_FORM}
    case(CLOSE_FORM): return{type: OPEN_FORM}   
  }
}
const store=createStore(rootReducer);    
class Form extends React.Component{
  constructor(props){
    super(props);
    this.state={name: '', phone: '', region: 'reg', comment : ''};
    this.hChange = this.hChange.bind(this);
        this.hSubmit = this.hSubmit.bind(this);

        
  }

  hChange(e){
    const {name, value} = e.target;

        this.setState({[name]: value});
  }




  hSubmit(e){
    console.log(this.state);
    submitForm()
    e.preventDefault();
  }

  render(){
    return( 
      <div class="popup-fade" id="formMain" style={popupfadeStyle}>
          <div class="popup" id="popup" style={popupStyle}>
          <i class="fas fa-times fa-lg" id="popup-close" style={popupcloseStyle}></i>
          <form class="ajaxForm" action="https://formcarry.com/s/x9qsuxPvbHX" method="POST" accept-charset="UTF-8" onSubmit={this.hSubmit}>
            <div id="fields" style={fieldsStyle}>
              <div class="form-group">
                <input class="field" id="name" name="name" placeholder="ФИО" type="text" value={this.state.name} size="60" style={fieldsStyle} onChange={this.hChange} autoFocus required/>
              </div>
              <div class="form-group">
                <input class="field" id="phone" name="phone" placeholder="Телефон" type="phone" value={this.state.phone} size="60" style={fieldsStyle} onChange={this.hChange} required/>
              </div>
              <div class="form-group" >
                <select class="field" id="region" name="region" style={fieldsStyle} onChange={this.hChange} required >
                    <option value="" disabled selected id="reg">Регион</option>
                    <option>Республика Адыгея</option>
                    <option>Республика Карелия</option>
                    <option>Республика Коми</option>
                    <option>Алтайский край</option>
                    <option>Краснодарский край</option>
                    <option>Красноярский край</option>
                    <option>Ставропольский край</option>
                    <option>Волгоградская область</option>
                    <option>Ленинградская область</option>
                    <option>Челябинская область</option>
                    <option >Приморский край</option>
                    <option >Еврейская автономная область</option>
                    <option >Ненецкий автономный округ</option>
                    <option >Ханты-Мансийский автономный округ</option>
                    <option >Чукотский автономный округ</option>
                </select>
              </div>
              <div class="form-group" >
                <textarea class="field" placeholder="Сообщение" name="comment" rows="5" cols="60" id="comment" style={fieldsStyle, commentStyle} onChange={this.hChange}required/>
              </div>
            </div>
            <div id="form_button" style={form_buttonStyle}>
            <button class="btn butt3" type="submit" value="Send" style={btnStyle}>
                <span>Отправить</span>
            </button>
            </div>
          </form>
        </div>    
      </div>);
  }
  
}

  function get_promise() {
    return new Promise((resolve, reject) => {
          fetch('https://formcarry.com/s/x9qsuxPvbHX', {
              method: 'POST',
              headers: {'Content-Type': 'application/json', 'Accept': 'application/json'},
              body: JSON.stringify({name: localStorage.getItem("name"), region:localStorage.getItem("region"), phone: localStorage.getItem("phone"), comment: localStorage.getItem("comment")})
          })
          .then(response => {
              console.log(response);
              alert("Сообщение отправлено!"); 
              localStorage.clear();
                document.getElementById("name").value = "";
                document.getElementById("phone").value = "";
                document.getElementById("region").value = document.getElementById("reg").value;
                document.getElementById("comment").value = "";                
                
              resolve();
          })
          .catch(error => {
              console.log(error); 
              alert("Test error!"); 
              reject();
          })
      });
  }

async function submitForm() {
      console.log("First Flag");
      var x = await get_promise();
      console.log("Second Log");
  }
const ProopForm=(state)=>{

  return{
    type: state.type
  }
};
const WrapperdForm=connect(ProopForm)(Form);

export default Form;